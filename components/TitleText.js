import React from 'react';
import { Text, StyleSheet } from 'react-native';

import Fonts from '../constants/default-styles';

const TitleText = props => (
  <Text style={{ ...styles.title, ...props.style }}>{props.children}</Text>
);

const styles = StyleSheet.create({
  title: {
    fontFamily: Fonts.title.fontFamily,
    fontSize: Fonts.title.fontSize
  }
});

export default TitleText;
