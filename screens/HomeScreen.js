import React from 'react'
import { Button, View, Text, StyleSheet } from 'react-native'
import Colors from '../constants/colors';

const HomeScreen = ({navigation}) => {
  return (
    <View style={{ justifyContent:"center", alignItems:"center" }}>
        <Text style={{fontSize:20, fontWeight:"bold"}}>Lets play a game!</Text>
        <View style={styles.container}>
           <Text style={styles.hifen}>
                - Enter a number and let our robot guess the number.
            </Text>
            <Text style={styles.btn}>
                - Use buttons to indicate whether the robot should guess higher or lower that their current guess.
            </Text> 
        </View>
        <View style={styles.btn}>
            <Button title='Im ready to play'  color={Colors.primary} onPress={() => navigation.navigate("StartGameScreen")}/>
        </View>
        
    </View>

  )
}

export default HomeScreen

const styles = StyleSheet.create({
    container:{
        width: '90%',
        borderColor: Colors.primary,
        borderWidth: 2,
        borderRadius: 5,
        justifyContent: 'center',
        paddingHorizontal: 15,
        paddingVertical: 30,
        width: '90%',
        marginTop: 22
    },
    hifen :{
        fontSize:15,
        paddingVertical:5

    },
    btn:{
        marginTop:15
    }
})