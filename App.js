import React, { useState } from 'react';
import { StyleSheet, View, SafeAreaView } from 'react-native';
import * as Font from 'expo-font';
import AppLoading from 'expo-app-loading';

import {NavigationContainer, DefaultTheme } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import LoginScreen from './screens/LoginScreen';
import HomeScreen from './screens/HomeScreen';
import StartGameScreen from './screens/StartGameScreen';
import GameScreen from './screens/GameScreen';
import GameOverScreen from './screens/GameOverScreen';

const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  });
};
const Stack = createStackNavigator();
export default function App() {
  const [userNumber, setUserNumber] = useState();
  const [guessRounds, setGuessRounds] = useState(0);
  const [dataLoaded, setDataLoaded] = useState(false);

  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setDataLoaded(true)}
        onError={err => console.log(err)}
      />
    );
  }

  // const configureNewGameHandler = () => {
  //   setGuessRounds(0);
  //   setUserNumber(null);
  // };

  // const startGameHandler = selectedNumber => {
  //   setUserNumber(selectedNumber);
  // };

  // const gameOverHandler = numOfRounds => {
  //   setGuessRounds(numOfRounds);
  // };

  // let content = <StartGameScreen onStartGame={startGameHandler} />;

  // if (userNumber && guessRounds <= 0) {
  //   content = (
  //     <GameScreen userChoice={userNumber} onGameOver={gameOverHandler} />
  //   );
  // } else if (guessRounds > 0) {
  //   content = (
  //     <GameOverScreen
  //       roundsNumber={guessRounds}
  //       userNumber={userNumber}
  //       onRestart={configureNewGameHandler}
  //     />
  //   );
  // }

  return (
    <SafeAreaView style={{flex: 1}}>
        <NavigationContainer theme={MyTheme}>
        <Stack.Navigator>
            <Stack.Screen name="LoginScreen" component={LoginScreen} options={{headerShown: false}}/>
            <Stack.Screen name="HomeScreen" component={HomeScreen} options={{headerTitle: "Dashboard"}}/>
            <Stack.Screen name="StartGameScreen" component={StartGameScreen} options={{headerTitle: "Start Game"}}/>
            <Stack.Screen name="GameScreen" component={GameScreen} options={{headerTitle: "Game in action"}}/>
            <Stack.Screen name="GameOverScreen" component={GameOverScreen} options={{headerTitle: "Game Over"}}/>
        </Stack.Navigator> 
        </NavigationContainer>
    </SafeAreaView>
  );
}

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: '#fff'
  },
};

const styles = StyleSheet.create({
  screen: {
    flex: 1
  }
});
